#include<iostream>
#include<string>
using namespace std; 
class Publication {
  char *title; /*用 STL 的 string 也行，但我是用慣 C 語言方式的*/
  float price;
public:
  Publication() {
    this->title = NULL;
    price = 0;
  }
  virtual void getdata() {
    char buf[2048];
    printf("Enter title: ");
    scanf("%s", buf);
    if(this->title != NULL) {
      free(this->title);
    }
    this->title = (char*) malloc(strlen(buf));
    memcpy(this->title, buf, strlen(buf)+1);
    printf("Enter price: ");
    scanf("%f", &this->price);
  }
  virtual void putdata() {
    printf(" Title: %s, Price: %.1f ", this->title, this->price);
  }
};

class Book : public Publication {
  int pages;
public:
  virtual getdata() {
    this->Publication::getdata();
    printf("Enter pages: ");
    scanf("%d", &this->pages);
  }
  virtual putdata() {
    this->Publication::putdata();
    printf("Pages: %d ", this->pages);
  }
}  
